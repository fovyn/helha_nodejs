import express from 'express';
import CustomerController from '../controllers/customer.controller'

const router = express.Router();

router.get(['', '/list'], CustomerController.listAction);


export default router;
