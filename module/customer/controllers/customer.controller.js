class CustomerController {
    constructor() {
        this.customers = [
            {name: "Ovyn", firstname: "Flavian", isActive: true},
            {name: "Strimelle", firstname: "Aurélien", isActive: false},
        ];

        this.listAction = (req, res) => {
            res.render('customer/list', { customers: this.customers.filter(c => c.isActive)})
        };
    }
}

export default new CustomerController();
