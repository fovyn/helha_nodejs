class BookController {
    constructor() {
        this.books = [{title: "StarWars"}, {title: "The Lords Of The Rings"}];

        this.listAction = (req, res) => {
            console.log(this.books);
            res.render('book/list', {books: this.books});
        };

        this.showAction = (req, res) => {
            console.log(req.params);
            const title = req.params.title;
            res.render('book/show', { book: this.books.find((b) => b.title === title)})
        };

        this.createAction = ({body: {title}}, res) => {
            console.log(title);
            this.books.push({title});
            res.render('book/list', {books: this.books});
        }
    }
}

export default new BookController();
