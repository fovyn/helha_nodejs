import express from 'express';
import BookController from '../controllers/book.controller';

const router = express.Router();

router.get(['', '/'], BookController.listAction);
router.get(['/:title', '/getOneByTitle/:title'], BookController.showAction);
router.post(['', '/'], BookController.createAction);


export default router;
