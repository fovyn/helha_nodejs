import express from 'express';
import path from 'path'
import BookRouter from '../module/book/routes/book.route';
import CustomerModule from '../module/customer/customer.module';

console.log(CustomerModule);

/**
 *
 * @type {app}
 */
const app = express();

app.use(express.urlencoded({extended: false}));

app.use('/book', BookRouter);
app.use('/customer', CustomerModule.Router);

app.set("views", path.join(__dirname, '../views'));
app.set("view engine", "pug");

app.get(["", "/", "/home"], (req, res) => {
    res.render('index');
});

app.listen(3000, () => console.log("Le serveur a démarré à l'url http://localhost:3000"));
